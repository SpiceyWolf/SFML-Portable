﻿using System.Reflection;

[assembly: AssemblyTitle("SFML.Portable.Winforms")]
[assembly: AssemblyDescription("SFML Portable Winforms Extension.")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyVersion("2.5.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("SFML.Net 2.5")]