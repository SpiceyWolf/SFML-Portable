﻿using System.Reflection;

[assembly: AssemblyTitle("SFML.Portable.Gtk")]
[assembly: AssemblyDescription("SFML Portable Gtk Extension.")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyVersion("2.5.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("SFML.Net 2.5")]