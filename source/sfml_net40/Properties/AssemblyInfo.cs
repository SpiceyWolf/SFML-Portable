﻿using System.Reflection;

[assembly: AssemblyTitle("SFML.Portable")]
[assembly: AssemblyDescription("SFML.Net combined to one dll. See Portable functions.")]
[assembly: AssemblyCompany("laurent@sfml-dev.org")]
[assembly: AssemblyCopyright("Copyright (C) 2007-2019 Laurent Gomila")]
[assembly: AssemblyVersion("2.5.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("SFML.Net 2.5")]